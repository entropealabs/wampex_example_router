# WampexExampleRouter

This is a simple application that creates a 3 node cluster of [WAMPexRouter](https://gitlab.com/entropealabs/wampex_router) nodes.

Persistant data is stored in CockroachDB in a directory local to the project directory.

## Up and running

To get started clone this repo. 
```bash
git clone https://gitlab.com/entropealabs/wampex_example_router.git
```
It utilizes [asdf-vm](https://github.com/asdf-vm/asdf) for tool management, so make sure you have it installed and have added the [docker-compose](https://github.com/virtualstaticvoid/asdf-docker-compose), [Erlang](https://github.com/asdf-vm/asdf-erlang) and [Elixir](https://github.com/asdf-vm/asdf-elixir) plugins.

Once asdf and the plugins are installed

```bash
cd wampex_example_router
asdf install
```

Erlang can take a minute to install if you don't have it already.

Once everything is installed run

```bash
docker-compose up --build
```

You should see the compilation happen, and then 3 nodes connect.

You now have a high availability WAMP Router running on ports 4000 and 4001.

The first node to start will create a default password for our admin interface, be sure to make note of it. You will need it to access the cluster.

Checkout the [wampex_example_app](https://gitlab.com/entropealabs/wampex_example_app) for a quick example of writing an Elixir client, you'll need that first password created.

Happy hacking ;)


