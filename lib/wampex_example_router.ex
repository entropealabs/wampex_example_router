defmodule WampexExampleRouter do
  @moduledoc """
  Documentation for Router.
  """
  use Application

  require Logger

  alias Wampex.Crypto
  alias Wampex.Router

  def start(_type, _opts) do
    topologies = Application.get_env(:wampex_example_router, :topologies)
    realm = get_sys_arg("ADMIN_REALM", "admin")
    authid = get_sys_arg("ADMIN_AUTHID", "admin")
    password = get_sys_arg("ADMIN_PASSWORD", Crypto.random_string(32))

    children = [
      {Router,
       [
         name: ExampleRouter,
         port: 4000,
         topologies: topologies,
         replicas: 3,
         quorum: 1,
         admin_realm: realm,
         admin_authid: authid,
         admin_password: password
       ]}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: ExampleRouter.Supervisor)
  end

  defp get_sys_arg(key, default) do
    case System.get_env(key) do
      nil ->
        Logger.warn("#{key} not available in environment, using: #{default}")
        default

      val ->
        val
    end
  end
end
