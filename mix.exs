defmodule WampexExampleRouter.MixProject do
  use Mix.Project

  def project do
    [
      app: :wampex_example_router,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      releases: releases(),
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {WampexExampleRouter, []}
    ]
  end

  defp releases do
    [
      app: [
        include_executables_for: [:unix],
        applications: [wampex_example_router: :permanent, runtime_tools: :permanent],
        cookie: "cluster_test_cookie"
      ]
    ]
  end

  defp deps do
    [
      # {:wampex_router, path: "../../wampex_router"}
      {:wampex_router,
       git: "https://gitlab.com/entropealabs/wampex_router.git",
       tag: "d00dff974b8362a77a0b7870751060f2cdc5432b"}
    ]
  end
end
