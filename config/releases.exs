import Config

config :wampex_router, Wampex.Router.Authentication.Repo,
  hostname: System.get_env("AUTH_DATABASE_HOSTNAME")
