use Mix.Config
config :logger, level: :info

config :wampex_router, Wampex.Router.Authentication.Repo,
  database: "authentication",
  hostname: "localhost",
  port: 26_257,
  username: "root"

config :wampex_example_router,
  topologies: [
    kv: [
      strategy: Cluster.Strategy.Epmd,
      config: [
        hosts: [
          :"app@test1.com",
          :"app@test2.com",
          :"app@test3.com"
        ]
      ],
      connect: {:net_kernel, :connect_node, []},
      disconnect: {:erlang, :disconnect_node, []},
      list_nodes: {:erlang, :nodes, [:connected]}
    ]
  ]
